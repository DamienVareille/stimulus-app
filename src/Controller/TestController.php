<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('test')]
class TestController extends AbstractController
{
    #[Route('/', name: 'test_controller', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $controllerName = $this?->getUser()?->getPassword();

        return $this->render('tests/index.html.twig', [
            'controller_name' => $controllerName ?? 'none'
        ]);
    }
}
