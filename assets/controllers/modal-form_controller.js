import {Controller} from "stimulus";
import {Modal} from "bootstrap";
import $ from 'jquery';
import {useDispatch} from "stimulus-use";

export default class extends Controller {

    static targets = ['modal', 'modalBody'];
    static values = {
        formUrl : String,
    }

    modal = null;

    connect() {
        useDispatch(this);
    }

    openModal(event) {
        this.modal = new Modal(this.modalTarget);
        this.modal.show();

        $.ajax(this.formUrlValue).then((r) => {
            this.modalBodyTarget.innerHTML = r;
        });
        console.log(this.dispatch('show'));
    }

    async submitForm(event) {
        event.preventDefault();
        const $form = $(this.modalBodyTarget).find('form');

        try {
            this.modalBodyTarget.innerHTML = await $.ajax({
                url: this.formUrlValue,
                method: $form.prop('method'),
                data: $form.serialize(),
            })
            this.modal.hide();
            this.dispatch('success');

        }catch (e) {
            this.modalBodyTarget.innerHTML = e.responseText;
        }
    }

    modalHidden() {

    }

    setUpFormModal() {
        console.log('coucou');
        $('#product_name').focus();
    }
}